package main

import (
	"flag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"sync"
)

var version = "undefined"

// Known ATA S.M.A.R.T. attributes
// https://en.wikipedia.org/wiki/S.M.A.R.T.#Known_ATA_S.M.A.R.T._attributes
// int - metric_name
var knownAttrs = map[int]string {
	1    : "read_error_rate",
	2    : "throughput_perfomance", 
	3    : "spinup_timne",
	4    : "start_stop_count",
	5    : "reallocated_sectors_count",
	6    : "read_channel_margin",
	7    : "seek_error_rate",
	8    : "seek_time_performance",
	9    : "poweron_hours",
	10   : "spin_retry_count",
	11   : "recallibration_retries",
	12   : "power_cycle_count",
	13   : "soft_read_error_rate",
	22   : "current_helium_level",
	170  : "available_reserved_space",
	171  : "ssd_program_fail_count",
	172  : "ssd_erase_fail_count",
	173  : "ssd_wear_leveling_count",
	174  : "unexpected_power_loss_count",
	175  : "power_loss_protection_failure",
	176  : "erase_fail_count",
	177  : "wear_range_delta",
	179  : "used_reserved_block_count_total",
	180  : "unused_reserved_block_count_total",
	181  : "program_fail_count_total",
	182  : "erase_fail_count",
	183  : "downshift_error_count",
	184  : "endtoend_error",
	185  : "head_stability",
	186  : "induced_opvibration_detection",
	187  : "reported_uncorrectable_errors",
	188  : "command_timeout",
	189  : "high_fly_writes",
	190  : "airflow_temperature",
	191  : "gsensor_error_rate",
	192  : "unsafe_shutdown_count",
	193  : "load_cycle_count",
	194  : "temperature",
	195  : "hardware_ecc_recovered",
	196  : "reallocation_event_count",
	197  : "current_pending_sector_count",
	198  : "uncorrectable_sector_count",
	199  : "ultradma_crc_error_count",
	200  : "multizone_error_rate",
	201  : "soft_read_error_rate",
	202  : "data_address_mark_errors",
	203  : "run_out_cancel",
	204  : "soft_ecc_correction",
	205  : "thermal_asperity_rate",
	206  : "flying_height",
	207  : "spin_high_current",
	208  : "spin_buzz",
	209  : "offline_seek_performance",
	210  : "vibration_during_write",
	211  : "vibration_during_write",
	212  : "shock_during_write",
	220  : "disk_shift",
	221  : "gsense_error_rate",
	222  : "loaded_hours",
	223  : "load_unload_retry_count",
	224  : "load_friction",
	225  : "load_unload_cycle_count",
	226  : "load_in_time",
	227  : "torque_amplification_count",
	228  : "poweroff_retract_cycle",
	230  : "drive_life_protection_status",
	231  : "life_left",
	232  : "endurance_remaining",
	233  : "poweron_hours",
	234  : "average_erase_count",
	235  : "good_block_count",
	240  : "head_flying_hours",
	241  : "total_lbas_written",
	242  : "total_lbas_read",
	243  : "total_lbas_written_expanded",
	244  : "total_lbas_read_expanded",
	249  : "nand_writes",
	250  : "read_error_retry_rate",
	251  : "minimum_spare_remaining",
	252  : "newly_added_bad_flash_block",
	254  : "free_fall_protection",
}

func main() {
	
	var (
		listenAddress = flag.String("web.listen-address", ":9257", "Address to listen on for web interface and telemetry.")
		metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
		utilityPath   = flag.String("smartctl.utility-path", "smartctl", "Path of the ssacli utility.")
		logLevel      = flag.String("loglevel", "info" , "Log level: fatal, error, warning, info, debug.")
	)
	flag.Parse()
	
	level, err := log.ParseLevel(*logLevel)
	if err != nil{
		level = log.InfoLevel
		log.Error("Couldn't  parse logLevel, set default - Info")
	}
	log.SetLevel(level)
	log.Info("Starting smart_exporter (" + version + ")")

	exporter, err := newSMARTExporter(*utilityPath)
	if err != nil {
		log.Fatal(err)
	}
	prometheus.MustRegister(exporter)

	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`
			<html>
			<head><title>S.M.A.R.T. Exporter</title></head>
			<body>
			<h1>S.M.A.R.T. Exporter</h1>
			<p><a href='` + *metricsPath + `'>Metrics</a></p>
			</body>
			</html>`))
	})
	log.Info("Listening on address:port => ", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}

//Define a struct for you collector that contains pointers
//to prometheus descriptors for each metric you wish to expose.
//Note you can also include fields of other types if they provide utility
//but we just won't be exposing them as metrics.
type SMARTExporter struct {
	collectLock sync.Mutex
	utilityPath string
}

//You must create a constructor for you collector that
//initializes every descriptor and returns a pointer to the collector
func newSMARTExporter(utilityPath string) (*SMARTExporter, error) {
	return &SMARTExporter{
		utilityPath: utilityPath,
	}, nil
}

var (
	SMARTctlScrapeSuccessDesc = prometheus.NewDesc(
		prometheus.BuildFQName("smartctl", "", "scrape_success"),
		"Whether scraping the S.M.A.R.T. stats was successful.",
		nil, nil)
	SMARTctlErrorsDesc = prometheus.NewDesc(
		prometheus.BuildFQName("smartctl", "", "errors"),
		"Errors in the scraping S.M.A.R.T.",
		[]string{"device", "message", "severity"}, nil)
)


//Each and every collector must implement the Describe function.
//It essentially writes all descriptors to the prometheus desc channel.
func (e *SMARTExporter) Describe(ch chan<- *prometheus.Desc) {
	//Update this section with the each metric you create for a given collector

	ch <- SMARTctlScrapeSuccessDesc
	ch <- SMARTctlErrorsDesc
}

//Collect implements required collect function for all promehteus collectors
func (e *SMARTExporter) Collect(ch chan<- prometheus.Metric) {
	//Implement logic here to determine proper metric value to return to prometheus
	//for each descriptor or call other functions that do so.
	e.collectLock.Lock()
	err := collectFromUtility(e.utilityPath, ch)
	e.collectLock.Unlock()
	
	if err == nil {
		ch <- prometheus.MustNewConstMetric(SMARTctlScrapeSuccessDesc, prometheus.GaugeValue, 1.0)
	} else {
		log.Error("Failed to gather stats: ", err)
		ch <- prometheus.MustNewConstMetric(SMARTctlScrapeSuccessDesc, prometheus.GaugeValue, 1.0)
	}
}


func collectFromUtility(utilityPath string, ch chan<- prometheus.Metric) error {
	smartCtl = utilityPath
	
	for dev := range setDevices() {
		variableLabels := []string{"dev"}
		// add MetaInformation
		ch <- prometheus.MustNewConstMetric(
					prometheus.NewDesc(
						prometheus.BuildFQName("smartctl", "", "meta"),
						"",
						variableLabels,
						prometheus.Labels{
							"firmware" : dev.Info.FirmwareVersion,
							"model" : dev.Info.DeviceModel,
							"serial" :dev.Info.SerialNumber,
						},
					),
					prometheus.GaugeValue,
					1.0,
					dev.Path)
		if dev.Info.IsSSD {
			ch <- prometheus.MustNewConstMetric(
						prometheus.NewDesc(
							prometheus.BuildFQName("smartctl", "", "is_ssd"),
							"",
							variableLabels,
							prometheus.Labels{},
						),
						prometheus.GaugeValue,
						1.0,
						dev.Path)
		} else {
			ch <- prometheus.MustNewConstMetric(
						prometheus.NewDesc(
							prometheus.BuildFQName("smartctl", "", "is_ssd"),
							"",
							variableLabels,
							prometheus.Labels{},
						),
						prometheus.GaugeValue,
						0.0,
						dev.Path)
		}
		ch <- prometheus.MustNewConstMetric(
					prometheus.NewDesc(
						prometheus.BuildFQName("smartctl", "", "capacity_bytes"),
						"",
						variableLabels,
						prometheus.Labels{},
					),
					prometheus.GaugeValue,
					float64(dev.Info.UserCapacityBytes),
					dev.Path)
		for _, attr := range dev.Attributes {
			attrName, isset := knownAttrs[attr.ID]
			if isset {
				metricName := prometheus.BuildFQName("smartctl", "", attrName)
				constLabels := prometheus.Labels{"ID" : strconv.Itoa(attr.ID), "attrNmae" : attr.Name}
				ch <- prometheus.MustNewConstMetric(
					prometheus.NewDesc(metricName, "", variableLabels, constLabels),
					prometheus.GaugeValue,
					float64(attr.Value),
					dev.Path)	
			}
		}
	}
	return nil
}