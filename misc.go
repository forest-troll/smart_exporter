package main

import (
	"bytes"
	log "github.com/sirupsen/logrus"
	"os/exec"
	"regexp"
	"strings"
	"sync"
)

var deviceRgx = regexp.MustCompile(`(/dev/[sh]d[a-z]+) -d (\S+)`)
var smartCtl string
var wg sync.WaitGroup


func cmdSMARTCtl(args ...string) (string, string, error) {
	var stdout, stderr bytes.Buffer
	cmd := exec.Command(smartCtl, args...)
	log.Debug("Running `%s with args: %v", smartCtl, args)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	
	log.Debug("%s: stdout `%s`, stderr `%s`", smartCtl, strings.TrimSpace(outStr), strings.TrimSpace(errStr))
	
	return outStr, errStr, err
}


func scanSMARTCtlDev() []string {
	//return array devname
	cmd, _, err := cmdSMARTCtl("--scan")
	if err != nil {
		log.Fatal(err)
	}
	lines := strings.Split(strings.TrimSpace(cmd), "\n")
	devices := make([]string, 0, len(lines))
	for _, line := range lines {
		tmp := strings.Split(line, "#")
		if m := deviceRgx.FindAllStringSubmatch(tmp[0], -1); m != nil {
			devices = append(devices, m[0][1])
		}
	}
	return devices
}


func infoSMARTCtlDev(devName string) []string {
	cmd, _, err := cmdSMARTCtl("-i", devName)
	if err != nil {
		log.Error(err)
	}
	lines := strings.Split(strings.TrimSpace(cmd), "\n")
	return lines
}

func attributesSMARTCtlDev(devName string) []string{
	cmd, _, err := cmdSMARTCtl("-A", devName)
	if err != nil {
		log.Error(err)
	}
	lines := strings.Split(strings.TrimSpace(cmd), "\n")
	return lines
} 

func setDevices() <- chan device{
	devNames := scanSMARTCtlDev()
	out := make(chan device, len(devNames))
	
	newDevice := func(devName string) {
		defer wg.Done()
		var d device
		d.Path = devName
		d.updateInfo()
		if d.Info.SMARTSupport {
			d.updateAttributes()
		}
		out <- d
		log.Debug("Add device ", devName, " complite")
		return
	}
	wg.Add(len(devNames))
	//devices := make ([]device, 0, len(devNames))
	for _, devName := range devNames {
		go newDevice(devName)
		//devices = append(devices, newDevice(devName))
	}
	log.Debug("Waiting")
	go func() {
		wg.Wait()
		close(out)
	}()
	log.Debug("Wait done")
	return out
	
}

