package main

import (
	"regexp"
	"strconv"
	"strings"
)


var (
	separateSectorSizesRgx     = regexp.MustCompile(`^(\d+) bytes logical, (\d+) bytes physical$`)
	sameSectorSizesRgx         = regexp.MustCompile(`^(\d+) bytes logical\/physical$`)
	userCapacityRgx            = regexp.MustCompile(`^([,\d]+) bytes \[.+?\]$`)
	rpmRgx                     = regexp.MustCompile(`^([\d]+) rpm$`)
	bytesRgx                   = regexp.MustCompile(`^(\d+) bytes$`)
	columnsRgx                 = regexp.MustCompile(`\s+`)
	influxDBFieldNameFilterRgx = regexp.MustCompile(`[^A-Za-z0-9]`)
)

type device struct {
	Path                  string
	Info                  deviceInfo
	Attributes[]          smartAttribute
	
}

type deviceInfo struct {
	DeviceModel           string
	SerialNumber          string
	LUWWNDeviceID         string
	FirmwareVersion       string
	UserCapacity          string
	UserCapacityBytes     int64
	SectorSizes           string
	LogicalSectorSizes    int
	PhysicalSectorSizes   int
	RotationRate          string
	RotationRateRPM       int
	IsSSD                 bool
	ATAVersion            string
	SATAVersion           string
	SMARTSupportIs        string
	SMARTSupport          bool
	Vendor                string
	Product               string
	Revision              string
	LogicalBlockSize      string
	LogicalBlockSizeBytes int
	LogicalUnitID         string
	DeviceType            string
	Health                string
	Healthy               bool
}

type smartAttribute struct {
	ID                    int
	Name                  string
	Flag                  uint16
	Value                 int
	Worst                 int
	Thresh                int
	Type                  string
	Updated               string
	WhenFailed            string
	RawValue              int
	RawValueNotes         string
}




func (d *device) updateInfo(){
	smartctlLines := infoSMARTCtlDev(d.Path)
	for _, line := range smartctlLines {
		sliced := strings.SplitN(line, ":", 2)
		switch sliced[0] {
		case "Device Model":
			d.Info.DeviceModel = strings.TrimSpace(sliced[1])
		case "Serial Number":
			d.Info.SerialNumber = strings.TrimSpace(sliced[1])
		case "Serial number":
			d.Info.SerialNumber = strings.TrimSpace(sliced[1])
		case "LU WWN Device Id":
			d.Info.LUWWNDeviceID = strings.TrimSpace(sliced[1])
		case "Firmware Version":
			d.Info.FirmwareVersion = strings.TrimSpace(sliced[1])
		case "User Capacity":
			d.Info.UserCapacity = strings.TrimSpace(sliced[1])
			if m := userCapacityRgx.FindAllStringSubmatch(d.Info.UserCapacity, -1); m != nil {
				d.Info.UserCapacityBytes, _ = strconv.ParseInt(strings.Replace(m[0][1], ",", "", -1), 10, 64)
			}
		case "Sector Size":
			d.Info.SectorSizes = strings.TrimSpace(sliced[1])
			if m := sameSectorSizesRgx.FindAllStringSubmatch(d.Info.SectorSizes, -1); m != nil {
				d.Info.LogicalSectorSizes, _ = strconv.Atoi(m[0][1])
				d.Info.PhysicalSectorSizes = d.Info.LogicalSectorSizes
			}
		case "Sector Sizes":
			d.Info.SectorSizes = strings.TrimSpace(sliced[1])
			if m := separateSectorSizesRgx.FindAllStringSubmatch(d.Info.SectorSizes, -1); m != nil {
				d.Info.LogicalSectorSizes, _ = strconv.Atoi(m[0][1])
				d.Info.PhysicalSectorSizes, _ = strconv.Atoi(m[0][2])
			}
		case "Rotation Rate":
			d.Info.RotationRate = strings.TrimSpace(sliced[1])
			if m := rpmRgx.FindAllStringSubmatch(d.Info.RotationRate, -1); m != nil {
				d.Info.RotationRateRPM, _ = strconv.Atoi(m[0][1])
			}
			d.Info.IsSSD = d.Info.RotationRate == "Solid State Device"
		case "ATA Version is":
			d.Info.ATAVersion = strings.TrimSpace(sliced[1])
		case "SATA Version is":
			d.Info.SATAVersion = strings.TrimSpace(sliced[1])
		case "SMART support is":
			d.Info.SMARTSupportIs = strings.TrimSpace(sliced[1])
			d.Info.SMARTSupport = d.Info.SMARTSupportIs == "Enabled"
		case "Vendor":
			d.Info.Vendor = strings.TrimSpace(sliced[1])
		case "Product":
			d.Info.Product = strings.TrimSpace(sliced[1])
		case "Revision":
			d.Info.Revision = strings.TrimSpace(sliced[1])
		case "Logical block size":
			d.Info.LogicalBlockSize = strings.TrimSpace(sliced[1])
			if m := bytesRgx.FindAllStringSubmatch(d.Info.LogicalBlockSize, -1); m != nil {
				d.Info.LogicalBlockSizeBytes, _ = strconv.Atoi(m[0][1])
			}
		case "Logical Unit id":
			d.Info.LogicalUnitID = strings.TrimSpace(sliced[1])
		case "Device type":
			d.Info.DeviceType = strings.TrimSpace(sliced[1])
		case "SMART overall-health self-assessment test result":
			d.Info.Health = strings.TrimSpace(sliced[1])
			d.Info.Healthy = d.Info.Health == "PASSED"
		}
	}
}

func (d *device) updateAttributes(){
	smartctlLines := attributesSMARTCtlDev(d.Path)
	var tmp []smartAttribute
	inTable := false
	i := 0
	for _, line := range smartctlLines {
		if inTable {
			columns := columnsRgx.Split(strings.TrimSpace(line), 11)
			tmp = append(tmp, newAttribute(columns))
		} else {
			i++
		}
		if strings.HasPrefix(line, "ID#") {
			inTable = true
		}
	}
	d.Attributes = tmp
}

func newAttribute(col []string) (attr smartAttribute){
	attr.ID, _ = strconv.Atoi(col[0])
	attr.Name = col[1]
	flag, _ := strconv.ParseUint(strings.Replace(col[2], "0x", "", 1), 16, 16)
	attr.Flag = uint16(flag)
	attr.Value, _ = strconv.Atoi(col[3])
	attr.Worst, _ = strconv.Atoi(col[4])
	attr.Thresh, _ = strconv.Atoi(col[5])
	attr.Type = col[6]
	attr.Updated = col[7]
	if col[8] != "-" {
		attr.WhenFailed = col[8]
	}
	attr.RawValue, _ = strconv.Atoi(col[9])
	if len(col) > 10 {
		attr.RawValueNotes = col[10]
	}
	return attr
}